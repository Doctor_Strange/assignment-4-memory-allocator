#include "mem.h"
#include <assert.h>
#include <stdio.h>


#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_MAGENTA     "\x1b[35m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define TEST(function)                                       \
    do {                                                     \
        fprintf(stderr, ANSI_COLOR_MAGENTA "Running:" ANSI_COLOR_RESET "%s... ", #function);        \
        function();                                          \
        fprintf(stderr, ANSI_COLOR_GREEN "PASSED" ANSI_COLOR_RESET "\n"); \
    } while (0)

static void test_memory_allocation() {
    void* allocated_memory = _malloc(0);
    assert(allocated_memory != NULL && "Memory allocation failed");
    _free(allocated_memory);
}

static void test_release_single_block() {
    void* block1 = _malloc(64);
    assert(block1 != NULL && "Failed to allocate block 1");
    _free(block1);
}

static void test_release_multiple_blocks() {
    void* block1 = _malloc(64);
    void* block2 = _malloc(128);
    assert(block1 != NULL && "Failed to allocate block 1");
    assert(block2 != NULL && "Failed to allocate block 2");
    _free(block1);
    _free(block2);
}

static void test_extend_memory_region() {
    void* block1 = _malloc(4096);
    void* block2 = _malloc(8129);
    assert(block1 != NULL && "Allocation of first block failed");
    assert(block2 != NULL && "Allocation of second block failed, insufficient heap extension");
    _free(block1);
    _free(block2);
}

static void test_extend_memory_with_obstacle() {
    void* obstacle = _malloc(4096);
    void* block = _malloc(8192);
    assert(obstacle != NULL && "Failed to allocate obstacle");
    assert(block != NULL && "Failed to allocate block, obstacle not considered");
    _free(obstacle);
    _free(block);

}


int main() {
	heap_init(1024 * 1024);
    TEST(test_memory_allocation);
    TEST(test_release_single_block);
    TEST(test_release_multiple_blocks);
    TEST(test_extend_memory_region);
    TEST(test_extend_memory_with_obstacle);
    return 0;
}